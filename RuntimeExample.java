import java.lang.Runtime;
import java.io.*;
public class RuntimeExample {
    public static void main(String[] args) {
        // Get a reference to the Runtime class
        Runtime runtime = Runtime.getRuntime();

        // Print out the available processors
        int availableProcessors = runtime.availableProcessors();
        System.out.println("Available Processors: " + availableProcessors);

        // Run an external command
        try {
            Process process = runtime.exec("ls -l");
            // ... Handle the process as needed
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
